package P12_6;

/**
 * Created by Arnav on 11/7/14.
 */
public class Question {

    private int level;
    private int answer;
    private String question;

    public Question(int level) {
        if (level < 1 || level > 3)
            throw new IllegalArgumentException("Invalid level input");
        this.level = level;
        answer = -1;
        question = "";
        genQuestion();
    }

    public int getAnswer() {
        return answer;
    }

    public void genQuestion() {
        int first = (int) (1 + Math.random()*9);
        int second = (int) (1 + Math.random()*9);

        if (level == 1) {
            if (first + second >= 10) {
                first /= 2;
                second /= 2;
            }
            question += first + " + " + second;
            answer = first+second;
        }
        else if (level == 2) {
            question += first + " + " + second;
            answer = first+second;
        }
        else {
            if (first >= second) {
                question += first + " - " + second;
                answer = first - second;
            }
            else {
                question += second + " - " + first;
                answer = second - first;
            }
        }
    }

    public String toString() {
        return question;
    }
}
