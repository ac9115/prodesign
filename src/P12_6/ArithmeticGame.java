package P12_6;

/**
 * Created by Arnav on 11/7/14.
 */
public class ArithmeticGame {

    private Player p1;
    private int numTries;
    private Question question;

    public ArithmeticGame(Player p1) {
        this.p1 = p1;
        numTries = 0;
        question = new Question(p1.getLevel());
    }

    public Question getCurrentQuestion() {
        return question;
    }

    public Question generateQuestion() {
        question = new Question(p1.getLevel());
        return question;
    }

    public int getNumTries() {
        return numTries;
    }

    public void updateTries() {
        numTries++;
    }

    public void reset() {
        p1.reset();
    }

    public boolean hasWon() {
        return (p1.getScore() >= 5 && p1.getLevel() >= 3);
    }

    public boolean gameOver() {
        return numTries >= 2;
    }

    public boolean isRight(int userAnswer) {
        return userAnswer == question.getAnswer();
    }

    public void resetTries() {
        numTries = 0;
    }


}
