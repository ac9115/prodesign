package P12_6;

/**
 * Created by Arnav on 11/7/14.
 */
public class Player {

    private int score;
    private int level;

    private static final int LEVEL_UP_THRESHOLD = 5;

    public Player() {
        score = 0;
        level = 1;
    }

    public void updateScore() {
        score++;
    }

    public boolean canLevelUp() {
        return score == LEVEL_UP_THRESHOLD;
    }

    public void levelUp() {
        level++;
        score = 0;
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public String toString() {
        return "Score: " + score + "Level: " + level;
    }

    public void reset() {
        score = 0;
        level = 1;
    }
}
