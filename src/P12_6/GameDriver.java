package P12_6;
import java.util.*;

/**
 * Created by Arnav on 11/7/14.
 */
public class GameDriver {

    public static void main(String[] args) {

        Player p1 = new Player();
        ArithmeticGame game = new ArithmeticGame(p1);
        Scanner in = new Scanner(System.in);

        while(game.gameOver() == false && game.hasWon() == false) {

            if (game.getNumTries() == 0)
                System.out.println(game.generateQuestion());
            else
                System.out.println(game.getCurrentQuestion());
            System.out.print("Answer: ");
            int ans = in.nextInt();
            if (game.isRight(ans)) {
                System.out.println("Correct!");
                p1.updateScore();
                game.resetTries();
                if (!game.hasWon() && p1.canLevelUp())
                    p1.levelUp();
            }
            else {
                System.out.println("Wrong! Try again");
                game.updateTries();
            }

        }

        if (game.hasWon())
            System.out.println("Congratulations math star! you won");
        else {
            System.out.println("Sorry, you lost. Try again");
            game.reset();
        }


    }
}
