package P12_8;

/**
 * Created by Arnav on 11/8/14.
 */
public class Item {

    private String name;
    private double price;
    private int quantity;

    private final static int DEFAULT_ITEM_QUANTITY = 100;

    public Item(String name, double price, int quantity) {
        if (price < 0.05)
            throw new IllegalArgumentException("invalid price, price must be greater than a nickel");
        if (quantity < 0)
            throw new IllegalArgumentException("invalid quantity, quantity must be positive");
        if (name.length() == 0)
            throw new IllegalArgumentException("must have a valid name");

        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Item(String name, double price) {
        if (price < 0.05)
            throw new IllegalArgumentException("invalid price, price must be greater than a nickel");
        if (name.length() == 0)
            throw new IllegalArgumentException("must have a valid name");

        this.name = name;
        this.price = price;
        this.quantity = DEFAULT_ITEM_QUANTITY;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String toString() {
        return name + " : " + "Price : " + price + "Quantity : " + quantity;
    }
}
