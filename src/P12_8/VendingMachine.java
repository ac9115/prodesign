package P12_8;
import java.util.*;

/**
 * Created by Arnav on 11/8/14.
 */
public class VendingMachine {

    private ArrayList<Item> items;
    private double money;

    public VendingMachine(ArrayList<Item> items) {
        if (items == null)
            throw new NullPointerException("items is null!");
        this.items = items;
        money = 0;
    }

    public Item getItem(int index) {
        if (index < 0 || index >= items.size())
            throw new IndexOutOfBoundsException("invalid index");
        else
            return items.get(index);
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public int getNumAvailableItems() {
        return items.size();
    }

    public void restock(Item item) {
        if (items.contains(item)) {
            int i = items.indexOf(item);
            items.get(i).setQuantity(100);
        }
        else
            System.out.println("Item is not in the vending machine");
    }

    public void vend(Item item) {
        if (items.contains(item)) {
            int i = items.indexOf(item);
            System.out.println("you received a " + item.getName());
            items.get(i).setQuantity(item.getQuantity() - 1);
            updateMoney(item.getPrice());
        }
    }

    // private method to add money once an item is vended
    private void updateMoney(double price) {
        money += price;
    }

    public double getMoney() {
        return money;
    }

    public void removeMoney() {
        money = 0;
    }

    public boolean enoughMoneyInserted(double amount, Item item) {
        return amount >= item.getPrice();
    }

    public boolean inStock(Item item) {
        if (!items.contains(item))
            return false;
        else {
            int i = items.indexOf(item);
            return items.get(i).getQuantity() > 0;
        }
    }

    public double processCoins(double[] payment) {
        double totalPayment = 0;
        for (int i = 0; i < payment.length; i++)
            totalPayment += payment[i];
        return totalPayment;
    }

    public void showItems() {
        for (int i = 0; i < items.size(); i++)
            System.out.println(i + ". " + items.get(i).getName() + " $" + items.get(i).getPrice());
    }

    public String toString() {
        String s = "";
        for (Item i : items)
            s += i + "\n";
        return s;
    }
}
