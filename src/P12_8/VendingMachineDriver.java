package P12_8;
import java.util.*;

/**
 * Created by Arnav on 11/8/14.
 */
public class VendingMachineDriver {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        VendingMachine drinks = generateVendingMachine();

        System.out.println("---------------------------------");
        drinks.showItems();
        System.out.println("---------------------------------");

        System.out.println();
        System.out.print("Enter coins (5, 10, or, 25) or type 'q' to quit: ");
        final String SENTINEL = "q";
        String coins = in.nextLine();

        while (!coins.equalsIgnoreCase(SENTINEL)) {
            String[] coinsArray = coins.split(" ");
            double[] payment = new double[coinsArray.length];

            for (int i = 0; i < coinsArray.length; i++) {
                try {
                    payment[i] = Double.parseDouble(coinsArray[i])/100;
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input for coins! " + e);
                }
            }

            double totalCoins = drinks.processCoins(payment);

            System.out.print("Enter your selection from " + 0 + " to " + (drinks.getNumAvailableItems() - 1) + ": ");
            int selection = in.nextInt();
            Item drinkSelected = null;

            try {
                drinkSelected = drinks.getItem(selection);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selection invalid! " + e);;
            }

            try {
                if (drinks.enoughMoneyInserted(totalCoins,drinkSelected) && drinks.inStock(drinkSelected))
                    drinks.vend(drinks.getItem(selection));
                else {
                    if (!drinks.enoughMoneyInserted(totalCoins,drinkSelected)) {
                        System.out.println("not enough money. Please add at least " + (drinkSelected.getPrice() - totalCoins) +
                                " dollars");
                    }
                    else {
                        System.out.println(drinkSelected.getName() + " is out of stock. Money returned");
                        drinks.restock(drinkSelected);
                    }
                }
            } catch (NullPointerException e) {
                System.out.println("Bah something went horribly wrong " + e);
            }

            in.nextLine();

            System.out.println();
            System.out.println("------------------------");
            drinks.showItems();
            System.out.println("------------------------");
            System.out.println();

            System.out.print("Enter coins (5, 10, or, 25) or type 'q' to quit: ");
            coins = in.nextLine();
        }

        System.out.println();
        System.out.println("****************************************************");
        System.out.println("Current amount of money in vending machine is $" + drinks.getMoney());
        System.out.println("****************************************************");

    }



    private static VendingMachine generateVendingMachine() {
        Item coke = new Item("Coke",1.00,100);
        Item sprite = new Item("Sprite",1.00,100);
        Item fanta = new Item("Fanta",1.00);
        Item drPepper = new Item("Dr Pepper",1.00,100);

        ArrayList<Item> items = new ArrayList<Item>();
        items.add(coke);
        items.add(sprite);
        items.add(fanta);
        items.add(drPepper);

        return new VendingMachine(items);
    }
}
